import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SampleServer {
    public static void main(String[] args) throws IOException {
        ServerSocket socket = new ServerSocket(8080);
        Socket clientSocket = socket.accept();
        if (clientSocket.isConnected()) {
            System.out.println("Connected");
        }
        OutputStream outputStream = clientSocket.getOutputStream();
        InputStream inputStream = clientSocket.getInputStream();

        DataInputStream dataInput = new DataInputStream(inputStream);
        DataOutputStream dataOutput = new DataOutputStream(outputStream);
        while (clientSocket.isConnected()) {
            if (clientSocket.isInputShutdown() || clientSocket.isOutputShutdown()) {
                break;
            }
            if (dataInput.available() > 0) {
                if (dataInput.readUTF().equals("start")) {
                    String message;
                    List<String> messageParts = new ArrayList<>();
                    int result = 0;
                    while (!(message = dataInput.readUTF()).equals("end")) {
                        messageParts.addAll(Arrays.asList(message.split(" ")));
                    }
                    System.out.println(messageParts);
                    if (messageParts.get(0).equals("sum")) {
                        try {
                            result = Integer.parseInt(messageParts.get(1)) + Integer.parseInt(messageParts.get(2));
                        } catch(NumberFormatException e){
                            e.printStackTrace();
                        }
                    }


                    dataOutput.writeUTF("start");
                    dataOutput.writeUTF(String.valueOf(result));
                    dataOutput.writeUTF("end");
                }
            }

        }
        clientSocket.close();
    }
}
