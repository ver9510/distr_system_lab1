import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket bank = new Socket("localhost", 8080);
        System.out.println("Connected");

        ObjectOutputStream objectOutput = new ObjectOutputStream(bank.getOutputStream());

        ObjectInputStream fromServerToClient = new ObjectInputStream(bank.getInputStream());

//        objectOutput.write(1);
//        while(fromClientToServer.read()<0);
//        objectOutput.writeObject(new StringBuilder("hello"));
        System.out.println(fromServerToClient.read());
        objectOutput.write(1);
        objectOutput.flush();

        bank.close();
    }
}
