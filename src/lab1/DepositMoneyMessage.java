package lab1;

import java.net.Socket;

public class DepositMoneyMessage extends Message {
    private int customerId;
    private int bill;

    public DepositMoneyMessage(int customerId, int bill, long timestamp) {
        super(timestamp, "depositMoney");
        this.customerId = customerId;
        this.bill = bill;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getBill() {
        return bill;
    }

    @Override
    public void processMessage(Bank bank, Socket shop) {
        System.out.println(this);
        bank.localTimestamp = this.getTimestamp() > bank.localTimestamp ? this.getTimestamp() + 1 : bank.localTimestamp + 1;
        Integer money = bank.customerAccounts.get(this.customerId);
        bank.depositMoney(customerId,bill, shop);
        bank.sendBroadcastMessage(new NullMessage(bank.localTimestamp+1));
    }

    @Override
    public String toString() {
        return "DepositMoneyMessage{" +
                "customerId=" + customerId +
                ", bill=" + bill +
                "} " + super.toString();
    }
}
