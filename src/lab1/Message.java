package lab1;

import java.io.Serializable;
import java.net.Socket;

public abstract class Message implements Serializable{
    private long timestamp;
    private String event;

    public Message(long timestamp, String event) {
        this.timestamp = timestamp;
        this.event = event;
    }

    public Message() {
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getEvent() {
        return event;
    }

    public abstract void processMessage(Bank bank, Socket shop);

    @Override
    public int hashCode() {
        return Long.valueOf(timestamp).intValue() + event.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if(!(obj instanceof Message)){
            return false;
        }
        return this.timestamp == ((Message) obj).timestamp && this.event.equals(((Message) obj).event);
    }

    @Override
    public String toString() {
        return "Message{" +
                "timestamp=" + timestamp +
                ", event='" + event + '\'' +
                '}';
    }
}
