package lab1;

import java.io.IOException;
import java.net.Socket;

public class Utils {
    public static void closeSocket(Socket socket) {
        try {
            socket.shutdownInput();
            socket.shutdownOutput();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
