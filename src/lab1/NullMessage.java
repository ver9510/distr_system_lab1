package lab1;

import java.net.Socket;

public class NullMessage extends Message {

    public NullMessage(long timestamp) {
        super(timestamp, "null");
    }

    @Override
    public void processMessage(Bank bank, Socket shop) {
        bank.localTimestamp = this.getTimestamp();
    }
}
