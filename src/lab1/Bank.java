package lab1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.util.Comparator.comparingLong;

public class Bank {
    long localTimestamp;
    private volatile PriorityQueue<Message> queue = new PriorityQueue<>(comparingLong(Message::getTimestamp));
    Map<Integer, Integer> customerAccounts = new HashMap<>();
    private ServerSocket socket;
    private List<Socket> shops = new ArrayList<>();
    private Map<Socket, ObjectInputStream> inputStreams = new HashMap<>();
    private Map<Socket, ObjectOutputStream> outputStreams = new HashMap<>();
    private Map<Socket, PriorityQueue<Message>> queues = new HashMap<>();
    private Map<PriorityQueue<Message>, Long> queueTimestamp = new HashMap<>();
    private Long minQueueTimestamp;

    private static volatile ExecutorService executorService;

    private void processQueue(PriorityQueue<Message> queue) {
        if (!queue.isEmpty()) {
            Message earliestMessage = queue.remove();
            if (queue.isEmpty()) {
                queueTimestamp.replace(queue, earliestMessage.getTimestamp());
            } else {
                queueTimestamp.replace(queue, queue.peek().getTimestamp());
            }
            if (earliestMessage.getEvent().equals("exit")) {
                return;
            }
            Socket shop = queues.entrySet().stream().filter(e -> e.getValue().equals(queue)).findAny().get().getKey();
            earliestMessage.processMessage(this, shop);
        }
    }

    private void getMessages(Socket shop) {
        ObjectInputStream inputStream = inputStreams.get(shop);
        while (!shop.isClosed()) {
            Message message = null;
            try {
                int read = inputStream.read();
                if (read > 0) {
                    Object o = inputStream.readObject();
                    message = (Message) o;
                } else {
                    break;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (message != null) {
                PriorityQueue<Message> queue = queues.get(shop);
                queue.add(message);
                if (queue.peek() != null) {
                    if (queueTimestamp.get(queue) != queue.peek().getTimestamp()) {
                        queueTimestamp.replace(queue, queue.peek().getTimestamp());
                    }
                }
                System.out.println("putted in bank queue " + message.toString());
            }
        }

    }

    private void sendMessageToShop(Socket shop, String event) {
        Message acceptMessage = new SimpleMessage(localTimestamp, event);
        sendMessage(acceptMessage, shop);
    }

    private void startNewClient(Socket acceptedSocket) {
        shops.add(acceptedSocket);
        if (acceptedSocket.isConnected()) {
            System.out.println("Connected " + acceptedSocket.toString());
        }
        try {
            PriorityQueue<Message> queue = new PriorityQueue<>(comparingLong(Message::getTimestamp));
            queues.put(acceptedSocket, queue);
            queueTimestamp.put(queue, 0L);
            outputStreams.put(acceptedSocket, new ObjectOutputStream(acceptedSocket.getOutputStream()));
            inputStreams.put(acceptedSocket, new ObjectInputStream(acceptedSocket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        executorService.submit(() -> getMessages(acceptedSocket));
    }

    private void findMinQueue() {
        while (true) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (isAllQueuesNotEmpty()) {
                PriorityQueue<Message> minQueue = getMinQueue();
                if (!minQueue.isEmpty()) {
                    executorService.submit(() -> processQueue(minQueue));
                    System.out.println("submitted");
                }
            }
        }
    }

    private boolean isAllQueuesNotEmpty() {
        return !queues.isEmpty() && queues.entrySet().stream().noneMatch(entry -> entry.getValue().isEmpty());
    }

    private PriorityQueue<Message> getMinQueue() {
        Optional<Map.Entry<PriorityQueue<Message>, Long>> min = queueTimestamp.entrySet()
                .stream()
                .min(Comparator.comparingLong(Map.Entry::getValue));
        return min.get().getKey();
    }

    private void init() {
        customerAccounts.put(1, 500);
    }

    private void sendMessage(Message message, Socket shop) {
        ObjectOutputStream outputStream = outputStreams.get(shop);
        try {
            outputStream.write(1);
            outputStream.writeObject(message);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendBroadcastMessage(Message message) {
        for (Socket socket : shops) {
            sendMessage(message, socket);
        }
    }

    public void acceptCredit(int customerId, int bill, Socket shop) {
        customerAccounts.replace(customerId, customerAccounts.get(customerId) - bill);
        System.out.println("Credit accepted! " + customerId + " " + customerAccounts.get(customerId));
        sendMessageToShop(shop, "creditAccepted");
    }

    public void rejectCredit(int customerId, int bill, Socket shop) {
        System.out.println("Credit request rejected! Not enough money on the account. \nRequested: " + bill + ". Money left: " + customerAccounts.get(customerId));
    }

    public void depositMoney(int customerId, int bill, Socket shop) {
        customerAccounts.replace(customerId, customerAccounts.get(customerId) + bill);
        System.out.println("Money deposited! " + customerId + " " + customerAccounts.get(customerId));
        sendMessageToShop(shop, "money put");
    }

    public static void main(String[] args) throws IOException {
        Bank bank = new Bank();
        bank.init();
        executorService = Executors.newFixedThreadPool(5);
        bank.socket = new ServerSocket(8080);
        executorService.submit(bank::findMinQueue);
        while (!bank.socket.isClosed()) {
            Socket acceptedSocket = bank.socket.accept();
            executorService.submit(() -> bank.startNewClient(acceptedSocket));
        }

        for (Socket socket : bank.shops) {
            Utils.closeSocket(socket);
        }
    }
}

