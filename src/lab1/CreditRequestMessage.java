package lab1;

import java.net.Socket;

public class CreditRequestMessage extends Message {
    private int customerId;
    private int bill;

    public CreditRequestMessage(int customerId, int bill, long timestamp) {
        super(timestamp, "creditRequest");
        this.customerId = customerId;
        this.bill = bill;
    }

    public int getCustomerId() {
        return customerId;
    }

    public int getBill() {
        return bill;
    }

    @Override
    public void processMessage(Bank bank, Socket shop) {
        System.out.println(this);
        bank.localTimestamp = this.getTimestamp() > bank.localTimestamp ? this.getTimestamp() + 1 : bank.localTimestamp + 1;
        Integer money = bank.customerAccounts.get(this.customerId);
        if (money > this.bill) {
            bank.acceptCredit(customerId, bill, shop);
        } else {
            bank.rejectCredit(customerId, bill, shop);
        }
        bank.sendBroadcastMessage(new NullMessage(bank.localTimestamp+1));
    }

    @Override
    public String toString() {
        return "CreditRequestMessage{" +
                "event=" + super.getEvent() +
                ", timestamp=" + super.getTimestamp() +
                ", customerId=" + customerId +
                ", bill=" + bill +
                '}';
    }
}
