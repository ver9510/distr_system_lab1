package lab1;

import java.util.Comparator;
import java.util.PriorityQueue;

public class Customer {
    private int id;
    private int localTimestamp = 0;
    private PriorityQueue<Message> queue = new PriorityQueue<>(Comparator.comparingLong(Message::getTimestamp));

    public Customer(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getLocalTimestamp() {
        return localTimestamp;
    }

    public PriorityQueue<Message> getQueue() {
        return queue;
    }
}
