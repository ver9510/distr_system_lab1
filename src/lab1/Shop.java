package lab1;

import java.io.*;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Shop {
    private long localTimestamp;
    private volatile PriorityQueue<Message> queue = new PriorityQueue<>(Comparator.comparingLong(Message::getTimestamp));
    private Socket bank;
    private static Queue<String> commands = new LinkedList<>();
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    private void processQueue() {
        while (true) {
            if (!queue.isEmpty()) {
                Message earliestMessage = queue.remove();
                switch (earliestMessage.getEvent()) {
                    case "creditAccepted": {
                        localTimestamp = earliestMessage.getTimestamp() + 1;
                        break;
                    }
                    case "null": {
                        localTimestamp = earliestMessage.getTimestamp();
                        sendMessage(new NullMessage(localTimestamp+1));
                        break;
                    }
                    case "exit": {
                        Utils.closeSocket(bank);
                        return;
                    }
                    default: {
                        System.out.println(earliestMessage);
                    }
                }
            }
        }
    }

    private void getMessages() {
        while (!bank.isClosed()) {
            Message message = null;
            try {
                int read = inputStream.read();
                System.out.println(read);
                if (read > 0) {
                    Object o = inputStream.readObject();
                    message = (Message) o;
                } else {
                    break;
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (message != null) {
                queue.add(message);
                System.out.println("putted in shop queue " + message.toString());
            }
        }
    }


    public static void main(String[] args) throws IOException, InterruptedException {
        Shop shop = new Shop();
        Customer customer = new Customer(1);
        shop.bank = new Socket("localhost", 8080);
        System.out.println("Connected");
        shop.inputStream = new ObjectInputStream(shop.bank.getInputStream());
        shop.outputStream = new ObjectOutputStream(shop.bank.getOutputStream());
        System.out.println("got streams");

        System.out.println("shop.getMessages");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(shop::getMessages);
        executorService.submit(shop::processQueue);

        Scanner scanner = new Scanner(System.in);
        String newCommand;
        while (!(newCommand = scanner.nextLine()).equals("exit")) {
            System.out.println("newCommand: " + newCommand);
            String[] split = newCommand.split(" ");
            String command = split[0];
            switch (command) {
                case "buy": {
                    Integer price = Integer.valueOf(split[1]);
                    shop.buy(customer, price);
                    break;
                }
                case "put": {
                    Integer money = Integer.valueOf(split[1]);
                    shop.putMoney(customer, money);
                    break;
                }
                case "addToLocaltimestamp":{
                    Integer time = Integer.valueOf(split[1]);
                    shop.localTimestamp+=time;
                    System.out.println("timestamp changed " +shop.localTimestamp );
                }
            }
        }
        commands.add("exit");
    }

    private void putMoney(Customer customer, Integer money) {
        Message depositMoneyMessage = new DepositMoneyMessage(customer.getId(), money, localTimestamp);
        localTimestamp++;
        System.out.println("sending " + depositMoneyMessage);
        sendMessage(depositMoneyMessage);
    }

    private void buy(Customer customer, int price) throws IOException, InterruptedException {
        Message creditRequestMessage = new CreditRequestMessage(customer.getId(), price, localTimestamp);
        localTimestamp++;
        System.out.println("sending " + creditRequestMessage);
        sendMessage(creditRequestMessage);
    }

    private void sendMessage(Message creditRequestMessage) {
        try {
            outputStream.write(1);
            outputStream.writeObject(creditRequestMessage);
            outputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
