import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.*;

public class SampleClient {
    private Queue<String> messages = new LinkedList<>();

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        SampleClient client = new SampleClient();
        Socket socket = new Socket("localhost", 8080);
        System.out.println("Connected");
        DataInputStream fromServerToClient = new DataInputStream(socket.getInputStream());
        DataOutputStream fromClientToServer = new DataOutputStream(socket.getOutputStream());


        Thread serverListener = new Thread(() -> {
            while (!socket.isClosed()) {
                try {
//                    if (fromServerToClient.available() > 0) {
                        String message = fromServerToClient.readUTF();
                        client.messages.add(message);
//                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        serverListener.setDaemon(true);
        serverListener.start();

        BufferedReader keyboardReader = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while (!(input = keyboardReader.readLine()).equals("exit")) {
            String command = input;
            if (command.equals("sum")) {
                String[] arguments = keyboardReader.readLine().split(" ");

                System.out.println("start");
                ExecutorService executorService = Executors.newSingleThreadExecutor();
                executorService.submit(() -> requestServer(fromClientToServer, command, arguments));
                System.out.println("end");

                Future<String> response = executorService.submit(() -> client.getResponse(socket));

                while (!response.isDone()) ;
                if (response.isDone()) {
                    System.out.println(response.get());
                }

            }
        }
        socket.close();
        if (socket.isClosed()) {
            System.out.println("Socket closed");
        }
        fromServerToClient.close();
        fromClientToServer.close();

    }

    private String getResponse(Socket socket) {
        String result = "";
        if (!messages.isEmpty()) {
            String message;
            if (messages.remove().equals("start")) {
                while (!(message = messages.remove()).equals("end")) {
                    result += message + " ";
                }
            }
        }
        return result;

    }

    private static void requestServer(DataOutputStream fromClientToServer, String command, String[] parameters) {
        try {
            fromClientToServer.writeUTF("start");

            fromClientToServer.writeUTF(command);

            for (String parameter : parameters) {
                fromClientToServer.writeUTF(parameter);
            }
            fromClientToServer.writeUTF("end");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
